var express = require('express')
var mongojs = require('mongojs')
var bodyParser = require('body-parser');
var app = express()
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
var db = mongojs('mongodb://localhost/espc2d')
var raw_data = db.collection('data')
app.get('/', function (req, res) {
  res.send('UP');
});
app.get('/anyupdate', (req,res)=>{
    raw_data.find({
        sent:false
    }).sort({_id:-1}, (err,doc)=>{
        if(doc[0]!=null){
            raw_data.update({_id:doc[0]._id},{$set:{sent:true}}, (err2,doc2)=>{
                res.send("1:"+doc[0].message);
            })
        }else{
            res.send("1");
        }
    });
})
app.get('/add/:message', (req,res)=>{
    raw_data.insert({
        message:req.params.message,
        sent: false
    }, (err,doc)=>{
        if(doc){
            res.json({
                stat:1
            })
        }else{
            res.json({
                stat:0
            })
        }
    });
})
app.post('/add', (req,res)=>{
    var body=req.body;
    var code=0;
    if(typeof body.chk_red != "undefined"){
        code=code|1;
    }
    if(typeof body.chk_green != "undefined"){
        code=code|2;
    }
    if(typeof body.chk_blue != "undefined"){
        code=code|4;
    }
    raw_data.insert({
        message:code,
        sent: false
    }, (err,doc)=>{
        if(doc){
            res.redirect('/dashboard');
        }else{
            res.json({
                stat:0
            })
        }
    });
})
db.on('connect', function () {
    console.log('database connected')
});

db.on('error', function (err) {
    console.log('database error', err)
});

app.use('/dashboard', express.static('public'));

app.listen(3000)